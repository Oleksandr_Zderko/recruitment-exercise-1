"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.app = void 0;
var server_ws_1 = require("./server-ws");
var app = new server_ws_1.ServerWs().getApp();
exports.app = app;
