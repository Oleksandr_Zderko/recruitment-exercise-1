"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ServerWs = void 0;
var WebSocket = require("ws");
var fs = require("fs");
var ServerWs = /** @class */ (function () {
    function ServerWs() {
        this.config();
        this.initialSocket();
        this.connectUser();
    }
    ServerWs.prototype.config = function () {
        this.port = process.env.PORT || ServerWs.PORT;
    };
    ServerWs.prototype.initialSocket = function () {
        this.socket = new WebSocket.Server({ port: this.port });
    };
    ServerWs.prototype.connectUser = function () {
        this.socket.on('connection', function (socket) {
            console.log('New user connected');
            var a = fs.readFileSync('../the-image.png', 'base64');
            socket.send(a);
        });
    };
    ServerWs.prototype.getApp = function () {
        return this.app;
    };
    ServerWs.PORT = 3000;
    return ServerWs;
}());
exports.ServerWs = ServerWs;
