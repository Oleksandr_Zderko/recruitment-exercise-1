import {createServer, Server} from 'http';
import * as express from 'express';
import * as WebSocket from 'ws';
import * as fs from 'fs';

export class ServerWs {
  public static readonly PORT: number = 3000;
  private app: express.Application;
  private port: string | number;
  private socket;

  constructor() {
    this.config();
    this.initialSocket();
    this.connectUser();
  }

  private config(): void {
    this.port = process.env.PORT || ServerWs.PORT;
  }

  private initialSocket(): void {
    this.socket = new WebSocket.Server({ port: this.port});
  }

  private connectUser(): void {
    this.socket.on('connection', (socket) => {
      console.log('New user connected');
      const a = fs.readFileSync('../the-image.png', 'base64');
      socket.send(a);
    })
  }

  public getApp(): express.Application {
    return this.app;
  }

}
