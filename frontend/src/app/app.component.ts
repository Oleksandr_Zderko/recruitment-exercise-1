import {Component, OnInit} from '@angular/core';
import {SocketService} from './services/socket.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  public image: string;

  constructor(private socketService: SocketService) {
  }

  ngOnInit(): void {
    this.socketService.initSocket();
    this.socketService.listenMessage().subscribe(image => {
      this.image = 'data:image/png;base64,' + image;
    });
  }
}
