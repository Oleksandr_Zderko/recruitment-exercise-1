import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SocketService {
  private socket;

  constructor() {
  }

  initSocket(): void {
    this.socket = new WebSocket('ws://localhost:3000');
  }

  listenMessage(): Observable<string> {
    return new Observable(observer => {
      this.socket.onmessage = (message) => {
        observer.next(message.data);
      };
    });
  }
}
